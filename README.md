# QGO Desktop Assessment

## Overview

Thanks for your interest a development role on the QGO Desktop team!

We'd like you to complete this short technical test before we bring you in for
an in person interview. It's a chance to show off your skills and show us how
you approach development.

This technical test is written in React and uses Redux for state management.
It assumes familiarity with React and Redux. If you aren't familiar with these
then let us know before starting the test!

You'll be working on a simple To Do app, extending it with a few new
features.

## Timing

About 90 mins.

If you don't get everything done in 90 mins don't worry, just send across what
you have with a note about what is left to do. We know your time is precious
and you don't want to spend all day on a technical test!

Given the time constrain we'd rather you focused on:
* Making it work, first and foremost!
* Unit tests.

Don't worry about making it look nice, or fancy CSS effects. We're happy with
unstyled buttons and elements. We'll be focusing on the code more than the
presentation.

## Getting Started

Install the required dependencies using npm:

```
npm install
```

You can then start the app with:

```
npm start
```

And run the tests (in watch mode) with:

```
npm test
```

## Tasks

We'd like you to make the following changes to the To Do app:

1. Add the ability to delete items.
2. Be able to mark items as complete. And then toggle them back to incomplete.
3. Add the ability to switch between viewing all items and only completed items.

## Finishing up

When you are finished with the test please send it back to us (either a link to
the repo, or a zip of the project).

Let us know if there are any improvements you would have made if given more time.

## Notes

Ideally this should feel as much like normal development as possible.

* Feel free to search for any information you need.
* You can add any libraries you need to get the job done.
* Please contact us if you have any questions about the task, we're happy to
  give more details!

## Updated by Developer

### What would I have done with more time?

* Used redux/reselect to have memoized selectors so that if the state tree grows, the visible items aren't recalculated on all state tree changes.
* Used CSS modules or at least a pre-proccessor for style composition (if the project had a larger scope)
* Better CSS selector naming (more uniform).
* Had a single connected component given the scope of this project (quite small)
* Moved the getVisibleItems() utility function to a separate utils/ folder with it's own spec file, rather than testing it in the itemsList.test.js spec file.

## Note

Took around 90 minutes, longer range of commit timestamps accounted for me having to eat dinner  :)
