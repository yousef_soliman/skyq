import React from 'react';
import { shallow } from 'enzyme';
import { ItemsList, getVisibleItems } from '../index';
import Item from '../../Item'
import ItemsVisibilityFilter from '../../ItemsVisibilityFilter'

const defaultProps = {
  items: [],
  // Functions that return empty strings to satisfy required prop type.
  handleRemove: () => '',
  handleToggleComplete: () => '',
  handleSetVisibilityFilter: () => ''
};

describe('ItemsList', () => {
  it('renders without crashing', () => {
    shallow(<ItemsList {...defaultProps} />);
  });

  it('should display warning message if no items', () => {
    const renderedItem = shallow(<ItemsList {...defaultProps} items={[]} />);
    expect(renderedItem.find('#items-missing')).toHaveLength(1);
  });

  it('should not display warning message if items are present', () => {
    const items = [{ id: 1, content: 'Test 1', completed: true }];
    const renderedItem = shallow(<ItemsList {...defaultProps} items={items} />);
    expect(renderedItem.find('#items-missing')).toHaveLength(0);
  });

  it('should render items as <Item> components', () => {
    const items = [{ id: 1, content: 'Test 1', completed: true}, { id: 2, content: 'Test 2', completed: true }];
    const renderedItem = shallow(<ItemsList {...defaultProps} items={items} />);
    expect(renderedItem.find(Item)).toHaveLength(2);
  });

  it('should render a <ItemsVisibilityFilter> component', () => {
    const renderedItem = shallow(<ItemsList {...defaultProps} />)
    expect(renderedItem.find(ItemsVisibilityFilter)).toHaveLength(1)
  })
});


describe('getVisibleItems', () => {
  it('should return items marked as completed if the second arg is SHOW_COMPLETED', () => {
    const prevItems = [
        {id: 1, content: 'text1', completed: true},
        {id: 2, content: 'text2', completed: false},
        {id: 3, content: 'text3', completed: false},        
      ]

    const items = getVisibleItems(prevItems, 'SHOW_COMPLETED')

    expect(items).toHaveLength(1)
    expect(items[0].completed).toBe(true)
  })

  it('should return all items if the second arg is SHOW_ALL', () => {
    const prevItems = [
        {id: 1, content: 'text1', completed: true},
        {id: 2, content: 'text2', completed: false},
        {id: 3, content: 'text3', completed: true},        
      ]

    const items = getVisibleItems(prevItems, 'SHOW_ALL')

    expect(items).toHaveLength(3)
  })
})