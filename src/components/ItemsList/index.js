import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { removeItem, toggleCompleteItem, setVisibilityFilter } from '../../logic/actions';
import Item from '../Item'
import ItemsVisibilityFilter from '../ItemsVisibilityFilter'
import './styles.css';

/**
 * Get visible items based on item.completed value and visibility filter.
 * @param {Array} items 
 * @param {String} filter 
 * @return {Array} items
 */
export const getVisibleItems = (items, filter) => {
  // Separate case for 'SHOW_ALL' rather than using default case for readability
  // and full scope of understanding incase we add more filters
  switch (filter) {
    case 'SHOW_ALL':
      return items
    case 'SHOW_COMPLETED':
      return items.filter(item => item.completed)
    default:
      return items
  }
}

export const ItemsList = ({ items, handleRemove, handleToggleComplete, handleSetVisibilityFilter }) => {
  return (
    <div>
      <ItemsVisibilityFilter handleSetVisibilityFilter={handleSetVisibilityFilter} />
      <ul className={'itemsList-ul'}>
        {items.length < 1 && <p id={'items-missing'}>Add some tasks above.</p>}
        {items.map(item => {
          const props = {...item, handleRemove, handleToggleComplete, key: item.id}
          return <Item {...props} />
        })}
      </ul>
    </div>
  );
};

ItemsList.propTypes = {
  items: PropTypes.array.isRequired,
  handleRemove: PropTypes.func.isRequired,
  handleToggleComplete: PropTypes.func.isRequired,
  handleSetVisibilityFilter: PropTypes.func.isRequired,
};

const mapStateToProps = state => {
  return {
    items: getVisibleItems(state.todos.items, state.todos.visibilityFilter)
  };
};

// Don't connect child <Item> component, leave that pure and connect this
// pass these bound actions down as props.
const mapDispatchToProps = dispatch => {
  return {
    handleRemove: id => dispatch(removeItem(id)),
    handleToggleComplete: id => dispatch(toggleCompleteItem(id)),
    handleSetVisibilityFilter: filter => dispatch(setVisibilityFilter(filter))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ItemsList);
