import React from 'react';
import { shallow, mount } from 'enzyme';
import { Item } from '../';


const defaultProps = {
  handleRemove: () => '',
  handleToggleComplete: () => '',
  id: 1,
  content: 'Item Text',
  completed: true,
}

const ITEM_COMPLETE_SELECTOR = 'span.itemText--complete'
const ITEM_INCOMPLETE_SELECTOR = 'span.itemText--incomplete'
const ITEM_REMOVE_BUTTON_SELECTOR = '.itemButton--remove'
const ITEM_TOGGLE_COMPLETE_BUTTON_SELECTOR = '.itemButton--toggle-complete'

describe('Item', () => {
  it('should render', () => {
    const renderedItem = shallow(<Item {...defaultProps} />)
    expect(renderedItem).toBeTruthy()
  })

  it('should render a list element', () => {
    const renderedItem = shallow(<Item {...defaultProps} />)

    expect(renderedItem.find('li')).toHaveLength(1)
  })

  it('should render a span element as a child of the list element with the text provided by prop: content', () => {
    const mockContent = 'Item Text'
    const renderedItem = shallow(<Item {...defaultProps} content={mockContent} />)

    expect(renderedItem.find('li span')).toHaveLength(1)
    expect(renderedItem.find('li span').text()).toBe(mockContent)
  })

  it('should render a span element that has a strike through if the item is completed', () => {
    const mockCompleted = true
    const renderedItem = shallow(<Item {...defaultProps} completed={mockCompleted} />)
  
    expect(renderedItem.find(ITEM_COMPLETE_SELECTOR)).toHaveLength(1)
  })

  it('should render a span element that has no strike through if the item is incomplete', () => {
    const mockCompleted = false
    const renderedItem = shallow(<Item {...defaultProps} completed={mockCompleted} />)
  
    expect(renderedItem.find(ITEM_INCOMPLETE_SELECTOR)).toHaveLength(1)
  })

  it('should render a button for deleting an item', () => {
    const renderedItem = shallow(<Item {...defaultProps} />)
    expect(renderedItem.find(ITEM_REMOVE_BUTTON_SELECTOR)).toHaveLength(1)
  })

  it('should render a button for toggling item completion', () => {
    const renderedItem = shallow(<Item {...defaultProps} />)
    expect(renderedItem.find(ITEM_TOGGLE_COMPLETE_BUTTON_SELECTOR)).toHaveLength(1)
  })

  it('should render a button for toggling item completion that reads "Mark Done" if the item is incomplete', () => {
    const renderedItem = shallow(<Item {...defaultProps} completed={false}/>)
    expect(renderedItem.find(ITEM_TOGGLE_COMPLETE_BUTTON_SELECTOR).text()).toBe('Mark Done')
  })

  it('should render a button for toggle item completion that reads "Mark Not Done" if the item is complete', () => {
    const renderedItem = shallow(<Item {...defaultProps} completed={true}/>)
    expect(renderedItem.find(ITEM_TOGGLE_COMPLETE_BUTTON_SELECTOR).text()).toBe('Mark Not Done')
  })

  it('should call the handleRemove prop with the item id when then delete button is clicked', () => {
    const mockHandleRemove = jest.fn()
    const renderedItem = mount(<Item {...defaultProps} id={2} handleRemove={mockHandleRemove}/>)

    renderedItem.find(ITEM_REMOVE_BUTTON_SELECTOR).simulate('click')

    const spyCalls = mockHandleRemove.mock.calls

    expect(spyCalls).toHaveLength(1)
    expect(spyCalls[0][0]).toBe(2)

  })

  it('should call the handleToggleComplete prop with then item id when the complete button is clicked', () => {
    const mockHandleToggleComplete = jest.fn()
    const renderedItem = mount(<Item {...defaultProps} id={2} handleToggleComplete={mockHandleToggleComplete}/>)

    renderedItem.find(ITEM_TOGGLE_COMPLETE_BUTTON_SELECTOR).simulate('click')

    const spyCalls = mockHandleToggleComplete.mock.calls

    expect(spyCalls).toHaveLength(1)
    expect(spyCalls[0][0]).toBe(2)
  })
})