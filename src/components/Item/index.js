import React from 'react';
import PropTypes from 'prop-types';
import './styles.css';

export const Item = ({handleRemove, handleToggleComplete, id, content, completed}) => {
  return(
    <li>
      <span className={completed ? 'itemText--complete' : 'itemText--incomplete'}>{content}</span>
      <a className={'itemButton--remove'} onClick={() => handleRemove(id)}>Remove</a>
      <a className={'itemButton--toggle-complete'} onClick={() => handleToggleComplete(id)}>{!completed ? 'Mark Done' : 'Mark Not Done'}</a>
    </li>
  )
}

Item.propTypes = {
  handleRemove: PropTypes.func.isRequired,
  handleToggleComplete: PropTypes.func.isRequired,
  id: PropTypes.number.isRequired,
  content: PropTypes.string.isRequired,
  completed: PropTypes.bool.isRequired
}

export default Item