import React from 'react';
import { shallow, mount } from 'enzyme';
import ItemsVisibilityFilter from '../';


const defaultProps = {
  handleSetVisibilityFilter: () => ''
}

const VISIBILITY_FILTER_SHOW_ALL_SELECTOR = '.visibilityFilter--show-all'
const VISIBILITY_FILTER_SHOW_COMPLETED_SELECTOR = '.visibilityFilter--show-completed'

describe('ItemsVisibilityFilter', () => {
  it('should render', () => {
    const renderedItem = shallow(<ItemsVisibilityFilter {...defaultProps} />)
    expect(renderedItem).toBeTruthy()
  })

  it('should render a button with the text "Show All"', () => {
    const renderedItem = shallow(<ItemsVisibilityFilter {...defaultProps} />)
    expect(renderedItem.find(VISIBILITY_FILTER_SHOW_ALL_SELECTOR)).toHaveLength(1)
  })

  it('should render a button with the text "Show Completed"', () => {
    const renderedItem = shallow(<ItemsVisibilityFilter {...defaultProps} />)
    expect(renderedItem.find(VISIBILITY_FILTER_SHOW_COMPLETED_SELECTOR)).toHaveLength(1)
  })

  it('should call handleSetVisibilityFilter with the arg "SHOW_ALL" when the "Show All" button is clicked', () => {
    const mockHandleSetVisibilityFilter = jest.fn()
    const renderedItem = mount(<ItemsVisibilityFilter {...defaultProps} handleSetVisibilityFilter={mockHandleSetVisibilityFilter}/>)
    
    renderedItem.find(VISIBILITY_FILTER_SHOW_ALL_SELECTOR).simulate('click')
    const spyCalls = mockHandleSetVisibilityFilter.mock.calls
    
    expect(spyCalls).toHaveLength(1)
    expect(spyCalls[0][0]).toBe('SHOW_ALL')
  })

  it('should call handleSetVisibilityFiltet with the arg "SHOW_COMPLETED" when the "Show Completed" button is clicked', () => {
    const mockHandleSetVisibilityFilter = jest.fn()
    const renderedItem = mount(<ItemsVisibilityFilter {...defaultProps} handleSetVisibilityFilter={mockHandleSetVisibilityFilter}/>)
  
    renderedItem.find(VISIBILITY_FILTER_SHOW_COMPLETED_SELECTOR).simulate('click')
    const spyCalls = mockHandleSetVisibilityFilter.mock.calls
    
    expect(spyCalls).toHaveLength(1)
    expect(spyCalls[0][0]).toBe('SHOW_COMPLETED')
  })
})