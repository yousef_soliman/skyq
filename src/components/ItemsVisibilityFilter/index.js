import React from 'react';
import PropTypes from 'prop-types';
import './styles.css';


export const ItemsVisibilityFilter = ({ handleSetVisibilityFilter }) => {
  return (
    <ul className={'visibilityFilter cf'}>
      <li>
        <a className={'visibilityFilter--show-all'} onClick={() => handleSetVisibilityFilter('SHOW_ALL')}>Show All</a>
      </li>
      <li>
        <a className={'visibilityFilter--show-completed'} onClick={() => handleSetVisibilityFilter('SHOW_COMPLETED')}>Show Completed</a>
      </li>
    </ul>
  )
}

ItemsVisibilityFilter.propTypes = {
  handleSetVisibilityFilter: PropTypes.func.isRequired,
}

export default ItemsVisibilityFilter