import reducer, { initialState } from '../reducer';
import { addItem, removeItem, toggleCompleteItem, setVisibilityFilter } from '../actions';

describe('reducer', () => {
  it('should return state for unknown action', () => {
    const mockState = { test: 'testItem' };
    const mockAction = { type: 'mystery-meat' };
    const result = reducer(mockState, mockAction);
    expect(result).toEqual(mockState);
  });

  it('should use initial state if state not provided', () => {
    const mockAction = { type: 'mystery-meat' };
    const result = reducer(undefined, mockAction);
    expect(result).toEqual(initialState);
  });

  it('should add new items on ADD_ITEM', () => {
    const mockAction = addItem('Test Content');
    const result = reducer(undefined, mockAction);
    expect(result.items).toHaveLength(3);
    expect(result.items[2].id === 3);
    expect(result.items[2].content === 'Test Content');
  });

  it('should remove existing items on REMOVE_ITEM', () => {
    const mockState = {
      items: [
        { id: 1, content: 'Make sure items are completeable' },
        { id: 2, content: 'Add filters (Use HOC)' },
      ],
      visibilityFilter: 'SHOW_ALL'
    }

    const mockAction = removeItem(1)
    const nextState = reducer(mockState, mockAction)

    const nextStateItems = nextState.items.filter(item => item.id === 1)
    expect(nextStateItems).toHaveLength(0)
  })

  it('should toggle an existing item as complete if not completed on TOGGLE_COMPLETE_ITEM', () => {
    const mockState = {
      items: [
        { id: 1, content: 'Make sure items are completeable', completed: false },
      ]
    }

    const mockAction = toggleCompleteItem(1)
    const nextState = reducer(mockState, mockAction)

    const isCompleted = nextState.items[0].completed
    expect(isCompleted).toBe(true)
  })

  it('should toggle an existing item as incomplete if already completed on TOGGLE_COMPLETE_ITEM', () => {
    const mockState = {
      items: [
        { id: 1, content: 'Make sure items are completeable', completed: true },
      ]
    }

    const mockAction = toggleCompleteItem(1)
    const nextState = reducer(mockState, mockAction)

    const isCompleted = nextState.items[0].completed
    expect(isCompleted).toBe(false)
  })

  it('should set the visibility filter on SET_VISIBILITY_FILTER', () => {
    const mockState = {
      visibilityFilter: 'SHOW_ALL'
    }

    const mockAction = setVisibilityFilter('SHOW_COMPLETED')
    const nextState = reducer(mockState, mockAction)

    expect(nextState.visibilityFilter).toBe('SHOW_COMPLETED')
  })

  it('should return previous state if visibility filter is not SHOW_ALL or SHOW_COMPLETED', () => {
    const mockState = {
      visibilityFilter: 'SHOW_ALL'
    }

    const mockAction = setVisibilityFilter('SHOW_DOESNT_EXIST')
    const nextState = reducer(mockState, mockAction)

    expect(nextState.visibilityFilter).toBe(mockState.visibilityFilter)
  })
});
