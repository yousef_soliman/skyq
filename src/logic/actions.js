import { ADD_ITEM, REMOVE_ITEM, TOGGLE_COMPLETE_ITEM, SET_VISIBILITY_FILTER } from './constants';

export const addItem = content => {
  return { type: ADD_ITEM, content };
};

/**
 * Remove an item based on item ID.
 * @param {Integer} id
 * @return {Object} action
 */
export const removeItem = id => {
  return {
    type: REMOVE_ITEM,
    id
  }
}

/**
 * Toggle an items complete value (boolean) based on item ID.
 * @param {Integer} id
 * @return {Object} action
 */
export const toggleCompleteItem = id => {
  return {
    type: TOGGLE_COMPLETE_ITEM,
    id
  }
}

export const setVisibilityFilter = filter => {
  // Validate that string passed as filter arg matches one of the values below
  // if not, early return an empty object
  const acceptedFilters = ['SHOW_ALL', 'SHOW_COMPLETED']
  if (acceptedFilters.indexOf(filter) === -1) {
    return {}
  }

  return {
    type: SET_VISIBILITY_FILTER,
    filter
  }
}