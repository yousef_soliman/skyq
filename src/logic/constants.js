export const ADD_ITEM = 'qgo/assessment/ADD_ITEM';
export const REMOVE_ITEM = 'qgo/assessment/REMOVE_ITEM'
export const TOGGLE_COMPLETE_ITEM = 'qgo/assessment/TOGGLE_COMPLETE_ITEM'
export const SET_VISIBILITY_FILTER = 'qgo/assessment/SET_VISBILITY_FILTER'