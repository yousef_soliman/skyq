import { ADD_ITEM, REMOVE_ITEM, TOGGLE_COMPLETE_ITEM, SET_VISIBILITY_FILTER } from './constants';

let nextId = 3;

export const initialState = {
  items: [
    { id: 1, content: 'Make sure items are completeable', completed: true},
    { id: 2, content: 'Add filters (Use HOC)', completed: false},
  ],
  visibilityFilter: 'SHOW_ALL'
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_ITEM:
      const newItem = {
        id: nextId++,
        content: action.content,
      };

      return {
        ...state,
        items: [...state.items, newItem],
      };
    case REMOVE_ITEM:
      return {
        ...state,
        items: state.items.filter(item => item.id !== action.id)
      };
    case TOGGLE_COMPLETE_ITEM:
      const newItems = state.items.map(item => {
        return action.id === item.id ? {...item, completed: !item.completed} : item
      })

      return {
        ...state,
        items: newItems
      }
    case SET_VISIBILITY_FILTER:
      return {
        ...state,
        visibilityFilter: action.filter
      }
    default:
      return state;
  }
};

export default reducer;
